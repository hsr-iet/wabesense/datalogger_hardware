================
Ordering Process
================

.. contents:: :local:

Needed Source Files
===================
	* PCB data
		* | Contains the complete layout file from `KiCad`_ project.
		  | :download:`datalogger.kicad_pcb <../../design/datalogger/datalogger.kicad_pcb>`
	* BOM data
		* | Bill of material information file for the PCB itself.
		  | .. note:: No external parts like
			* connectors needed for the user-Button/user-LED are part of this list!
			* CR2032 battery needed for RTC Backup are part of this list!
		  | :download:`datalogger_PCB_BOM.xlsx <../../design/datalogger_PCB_BOM.xlsx>`
	* Pick and Place data
		* | Contains the correct position and angle information for each part on the design. Use this, even if position information are part of KiCad layout file!
		  | :download:`WABEsense_Datalogger_v1_0_all-pos.xlsx <../../design/WABEsense_Datalogger_v1_0_all-pos.xlsx>`

`Eurocircuits`_
===============

If you need to reorder exactly the same PCB as already previous ordered, you could simply add the same order again by:
	- Click "Order repeats/view history"
	- Do only the step `Check uploaded PCB Data - Assembly Visualizer`_ to check component availability.

Calculate and place an order
----------------------------

1. Login
2. Click "Calculate and Order"
3. Click "Drop PCB Data Here" and add PCB-Design file
	- | "datalogger.kicad_pcb": :file:`design/datalogger/datalogger.kicad_pcb`
	  | :download:`datalogger.kicad_pcb <../../design/datalogger/datalogger.kicad_pcb>`
		
	.. image:: ../pictures/eurocircuits_ordering/datalogger_PCB_Ordering_Eurocircuits_Upload_PCB_data.jpg
	  :width: 750
	  :alt: Datalogger ordering on `Eurocircuits`_: PCB data upload
  
4. Click "Upload BOM data" and add BOM file
	- | Click "Browse" and add "datalogger_PCB_BOM.xlsx": :file:`design/datalogger_PCB_BOM.xlsx`
	  | :download:`datalogger_PCB_BOM.xlsx <../../design/datalogger_PCB_BOM.xlsx>`
		
	.. image:: ../pictures/eurocircuits_ordering/datalogger_PCB_Ordering_Eurocircuits_Upload_BOM_data.jpg
	  :width: 750
	  :alt: Datalogger ordering on `Eurocircuits`_: BOM data upload

5. Click "Upload CPL data" and add pick and Place data for automated component placing
	- | Click "Browse" and add "WABEsense_Datalogger_v1_0_all-pos.xlsx": :file:`design/WABEsense_Datalogger_v1_0_all-pos.xlsx`
	  | :download:`WABEsense_Datalogger_v1_0_all-pos.xlsx <../../design/WABEsense_Datalogger_v1_0_all-pos.xlsx>`
	
	.. image:: ../pictures/eurocircuits_ordering/datalogger_PCB_Ordering_Eurocircuits_Upload_CPL_data.jpg
	  :width: 750
	  :alt: Datalogger ordering on `Eurocircuits`_: CPL data upload

6. Click "Confirm Analysis"
	- In this step, the PCB will be checked by a automated design-rules-check by `Eurocircuits`_
	
7. Clock "Full PCB Analysis"
	- Complete PCB-Design is placed in shopping basket and analysis is start running
	- In the following input-mask, you could add specific informations for you (not shown on hardware)
		- PCB Name: Just used in ordering process, to identify your product
		- Purchase reference: Typical an internal finance number or the name of the person who place the order
		- Articel reference: If you are using reference number internaly for this specific PCB part
		- Project reference: Typical the name of the Project
		- Lead time: 9 Working days typical
		- PCB Quantity: The number of requested PCBs
		
		.. image:: ../pictures/eurocircuits_ordering/datalogger_PCB_Ordering_Eurocircuits_Confirm_Parameters.jpg
		  :width: 750
		  :alt: Datalogger ordering on `Eurocircuits`_: Confirm_Parameters
		
	- Click "Proceed to Analysis"
	- After this step, the "Shopping basket" website will be shown up
		- A couple of minutes are need to be waited, until the order is fully checked
		
		.. image:: ../pictures/eurocircuits_ordering/datalogger_PCB_Ordering_Eurocircuits_Shopping_basket.JPG
		  :width: 750
		  :alt: Datalogger ordering on `Eurocircuits`_: Shopping basket
		
Check uploaded PCB Data - PCB Visualizer
----------------------------------------

1. Click on the order to do some needed modifications and a manual check all over
2. Change value in Technology option for
	- Outer layer annular ring (OAR) to 0.100 mm (needed, for USB connector)

	.. image:: ../pictures/eurocircuits_ordering/datalogger_PCB_Ordering_Eurocircuits_PCB_Visualizer_Technology.JPG
	  :width: 750
	  :alt: Datalogger ordering on `Eurocircuits`_: Shopping basket

Check uploaded PCB Data - Assembly Visualizer
---------------------------------------------

In this process step, all components will be checked, if they are available on market to be ordered by `Eurocircuits`_
Click "PCBA Visualizer", located as second tab on top of current window

.. image:: ../pictures/eurocircuits_ordering/datalogger_PCB_Ordering_Eurocircuits_PCBA_Visualizer.JPG
	  :width: 750
	  :alt: Datalogger ordering on `Eurocircuits`_: PCBA Visualizer

1. Click on the "4 Part(s) not identified", so only these are visible (filterd parts).

	.. image:: ../pictures/eurocircuits_ordering/datalogger_PCB_Ordering_Eurocircuits_Components_not_identified.JPG
		  :width: 750
		  :alt: Datalogger ordering on `Eurocircuits`_: Components not identified
		  
	- Change for "JP1" "Supplied by" from "Assembler" to "Not placed" (to do so, click on "Assembler" and change it)
	- Change for "NT1" "Supplied by" from "Assembler" to "Not placed"
	- Change for "R48, R49" "Supplied by" from "Assembler" to "Not placed"
	- Change for "TP.." (Testpoints) "Supplied by" from "Assembler" to "Not placed"
		
2. Click on the "x Part(s) with no price info", so these are currently or not anymore available on market
	- On each part, a idividual solution needs to be found.
	
	Possible solutions/issues:
		- Part price is currently not available, but Parts are in eC-stock (`Eurocircuits`_ stock) --> no action required, only the price is currently not available.
		- Find a alternate part for this component
			- for some of parts, alternate factory numbers are listed in BOM file. Per default, part listed in "SPN" column will be used for odering process.
			- otherwise you need to find an alternate supplier by yourself and you could deliver the parts to `Eurocircuits`_ yourself
				- Click on "Find alternatives" to change part number
			- or find an other not already listed alternate part that matches the main requirements of the components
				- Package compatible
				- Have a look at "Details" column, which shows the importent requirements

Links
=====
.. _Eurocircuits: https://www.eurocircuits.com/
.. _KiCad: https://www.kicad.org/

.. target-notes::