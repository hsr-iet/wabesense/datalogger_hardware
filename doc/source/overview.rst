========
Overview
========

.. versionadded:: 1.0
	Zero series of hardware development.

.. contents:: :local:

Design Pictures
===============
Datalogger PCB top side.

.. image:: https://gitlab.com/hsr-iet/wabesense/wabesense/-/raw/master/doc/img/datalogger_zeroSeries_PCB_Top_annotated.png
  :width: 400
  :alt: image of the top side
  
Datalogger PCB bottom side.

.. image:: https://gitlab.com/hsr-iet/wabesense/wabesense/-/raw/master/doc/img/datalogger_zeroSeries_PCB_Bottom_annotated.png
  :width: 400
  :alt: image of the bottom side

|

Main features
=============

Design goals
------------
	
	* LowPower
		* Ultra Low Power Microcontroller, which is in Standby Mode as default and only active during measurement/logging
		* ON/OFF switchable power paths, to reduce system standby current
	
	* User friendly
		* Simple interaction between user and system
			* Buttons
			* LED for visual feedback
			* Data export by USB-Stick (USB Type-C)
			* Firmware update by USB-Stick (USB Type-C)
	
	* Easy configurable
		* Simple serial communication protocoll, to interact over USB Type-C connector and do some setup of	
		
	* Open Source Hardware
		* The data-logger is Open Hardware you can reuse and modify it. Please make sure that you understand the license before you use design, images, or source code. The development files are available on the hardware repository.
		* The data-logger was developed using FOSS software whenever possible, in particular we used `KiCad`_ for the PCB.

Interfaces
----------

	* User interface
		* Two buttons via connectors ()
		* Two color LED via connector
		* USB-C Port (J2)
		* SD card slot (unused in this project)
	* Power supply options
		* DC powersupply or Battery/Accumulator: via power Jack 2x5.5mm (J1, 5.0 to 28V, 0.5A)
		* USB: via USB Type C connector (J2)
	* Configuration/RTC Backup
		* CR2032 battery (BT1)
	* External measurement/interface channels
		* 4x Analog 0-5 V
		* 2x Analog 0-3.3 V (VDDA)
		* 7x Digital In/Out
		* SPI interface or additional 4x Digital In/Out
		* I2C interface or additional 2x Digital In/Out
	* Power outputs
		* V_Supply (USB or external DC-Adapter)
		* 3.3 V supply
		* 3.3 V supply switched by micro-controller
		* 17 V supply switched by micro-controller
		* 3.3 V VDDA supply 
	* Internal measurement/interface channels
		* `BME280`_ sensor
			* pressure, temperature, humidity
		* `AT25SF321B`_ internal data storage (4 MB FLASH)
			* up to 130000 data points (> 2 years @10 minute sampling interval)

Result
------
	* LowPower: Typical System-Standby Power consumption
		* 12V PowerIn results in 12-13uA

Dimensions
==========
	* Mounting holes: four mounting holes for M3 (3.2 mm hole) screws are placed in the corners of the PCB.
	*
		.. image:: ../pictures/WABEsense_PCB_dimensions.pdf
		  :width: 800
		  :alt: Datalogger PCB dimensions

.. list-table:: PCB dimensions
	:widths: 10 10 10
	:header-rows: 1
		
	*	- width
		- depth
		- height
	*	- 105.00 mm
		- 60.00 mm
		- 17.63 mm

PCB design tool
===============
The complete hardware design was created with `KiCad`_ 64-bit in version 5.1.6 (`KiCad_v516`_) under windows.

Links
=====
.. _BME280: https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/
.. _AT25SF321B: https://www.dialog-semiconductor.com/sites/default/files/2021-04/DS-AT25SF321B-179E-062020.pdf
.. _KiCad: https://www.kicad.org/
.. _KiCad_v516: https://kicad-downloads.s3.cern.ch/windows/stable/kicad-5.1.6_1-x86_64.exe

.. target-notes::