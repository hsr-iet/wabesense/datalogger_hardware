=============
ioDescription
=============

.. contents:: :local:


Datalogger internal (system)
============================

.. list-table:: I/Os used on the datalogger itself (system)
	:widths: 10 10 100
	:header-rows: 1
	
	*	- Microcontroller pin
		- Name
		- Details
	*	- PA0
		- SYS_WKUP_1_VDD_USB
		- System wakeup: VDD USB detection
	*	- PA1
		- ADC_IN6_V_BAT
		- Analog battery voltage measurement
	*	- PA2
		- SYS_WKUP_4_SDCard_Detection
		- System wakeup: SD-Card insert detection
	*	- PA7
		- USR_LED_R
		- HMI: Red user led
	*	- PA8
		- V_SupplyMeasEna
		- V_Supply analog voltage measurement path enable
	*	- PA9
		- VDD_USB
		- 3.3V VDD USB Voltage feedback
	*	- PA10
		- ENA_5V_USB
		- Used to enable 5V V_BUS supply from datalogger (used if USB Host)
	*	- PA11
		- USB DM
		- USB: Negative USB dataline
	*	- PA12
		- USB DP
		- USB: Positive USB dataline
	*	- PA13
		- SWDIO
		- SWD: Data in/output of serial wire debug interface
	*	- PA14
		- SWDCLK
		- SWD: Clock of serial wire debug interface
	*	- PA15
		- ENA_5V_EXT
		- Could be used to switch 5V DC/DC regulator U3 down (if USB Powered as Device)
	*	- PB0
		- USR_LED_G
		- HMI: Green user led
	*	- PB1
		- TP25
		- Testpoint only, usefull for debugging
	*	- PB2
		- TP16
		- Testpoint only, usefull for debugging
	*	- PB3
		- SPI1_SCK
		- SPI1: Clock signal of the SPI1 interface
	*	- PB4
		- SPI1_MISO
		- SPI1: Master in slave out signal of the SPI1 interface
	*	- PB5
		- SPI1_MOSI
		- SPI1: Master out slave in signal of the SPI1 interface
	*	- PB6
		- SPI1_FLASH_CSn
		- Digital Output: FLASH ic chipselect signal (used together with SPI1 interface)
	*	- PB7
		- SPI1_BMP280_CSn
		- Digital Output: BMP280 ic chipselect signal (used together with SPI1 interface)
	*	- PB8
		- SPI1_SDCard_CSn
		- SPI1: SD card chipselect signal (used together with SPI1 interface
	*	- PB9
		- ENA_3V3
		- Digital Output: Used to control the power of other 3.3V relevant components
	*	- PC2
		- ADC_IN3_USB_CC1
		- Used for USB Type-C to establish and manage Source-to-Sink connection.
	*	- PC3
		- ADC_IN4_USB_CC2
		- Used for USB Type-C to establish and manage Source-to-Sink connection.
	*	- PC4
		- ENA_17V
		- Digital Output: Used to control the power of the 17V relevant components
	*	- PC5
		- SYS_WKUP_5_USR_BT1
		- System wakeup of user button1
	*	- PC13
		- SYS_WKUP_2_USR_BT2
		- System wakeup of user button2
	*	- PC14
		- OSC32_IN
		- 32kHz Oscillator
	*	- PC15
		- OSC32_OUT
		- 32kHz Oscillator
	*	- PD2
		- OTG_Device
		- Digital output to select active pullup/pulldown-resistors on D+/D- lines of USB --> needed for USB Host or Device recognition
	*	- PH0
		- TP17
		- Testpoint only, usefull for debugging
	*	- PH1
		- TP18
		- Testpoint only, usefull for debugging
	*	- PH3
		- BOOT0
		- Firmware Update


External available I/Os
=======================

analog Inputs
-------------

.. list-table:: External available analog inputs
	:widths: 10 10 30 10 10 10
	:header-rows: 1
	
	*	- Microcontroller pin
		- External name
		- Analog input Voltage range
		- Pin on connector J5
		- Pin on connector J6 (sensor connectors)
		- Pin on connector J7 (sensor connectors)
	*	- PA3
		- ADC_IN8
		- 0 ... 5V
		- Pin 1
		- Pin 2
		- 
	*	- PA4
		- ADC_IN9
		- 0 ... 5V
		- Pin 3
		- Pin 3
		- 
	*	- PA5
		- ADC_IN10
		- 0 ... 5V
		- Pin 5
		- 
		- Pin 2
	*	- PA6
		- ADC_IN11
		- 0 ... 5V
		- Pin 7
		- 
		- Pin 3
	*	- PC0
		- PC0_ADC_IN1
		- 0 ... 3.3V
		- Pin 2
		- 
		- 
	*	- PC1
		- PC0_ADC_IN1
		- 0 ... 3.3V
		- Pin 4
		- 
		- 

digital I/Os
------------

.. list-table:: External available digital I/Os
	:widths: 10 10 50
	:header-rows: 1
	
	*	- Microcontroller pin
		- Pin on connector J5
		- Options / Notes
	*	- PB10
		- Pin 27
		- I2C2_SCL or simple digital in/output, see pullup-resistors R48 (not populated)
	*	- PB11
		- Pin 29
		- I2C2_SDA or simple digital in/output, see pullup-resistors R49 (not populated)
	*	- PB12
		- Pin 17
		- SPI2_NSS or simple digital in/output
	*	- PB13
		- Pin 19
		- SPI2_SCK or simple digital in/output
	*	- PB14
		- Pin 21
		- SPI2_MISO or simple digital in/output
	*	- PB15
		- Pin 23
		- SPI2_MOSI or simple digital in/output
	*	- PC0
		- Pin 2
		- ADC_IN1 or simple digital in/output
	*	- PC1
		- Pin 4
		- ADC_IN2 or simple digital in/output
	*	- PC6
		- Pin 18
		- simple digital in/output
	*	- PC7
		- Pin 20
		- simple digital in/output
	*	- PC8
		- Pin 22
		- simple digital in/output
	*	- PC9
		- Pin 24
		- simple digital in/output
	*	- PC10
		- Pin 26
		- simple digital in/output
	*	- PC11
		- Pin 28
		- simple digital in/output
	*	- PC12
		- Pin 30
		- simple digital in/output
		