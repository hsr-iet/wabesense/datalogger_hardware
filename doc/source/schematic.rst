=========
schematic
=========

.. contents:: :local:

Top
===
	* Power connector (J1)
	* USB C connector (J2)
		* USB 2.0 OTG (Host or Device (default))
	* SD-Card connector (J3)
	* Hirarchicel top files of other schematic pages

.. image:: ../pictures/schematic/WABEsense_Datalogger_Schematic-001.png
  :width: 800
  :alt: Datalogger top
  
Power
=====
	* 5V Power generation from external Power Input (J1)
	* Input Power selection
		* 5V generation or 5V from USB) --> this is the general system voltage, used for all other following parts
	* 3.3V Power generation
		* buck DC/DC-converter
		* 3.3V / max. 750mA
		* used for microcontroller, BME280
	* 17.0V Power generation (switched)
		* boost DC/DC-converter
		* 17V / max. 30mA
		* ON/OFF controlled by microcontroller (ENA_17V)
	* 3.3V Switched Voltage
		* ON/OFF controlled by microcontroller (ENA_3V3)
		* used for internal FLASH
	* 5.0V Switched Voltage
		* ON/OFF controlled by microcontroller (ENA_5V_USB)
		* Used to power V_USB if working as USB Host (e.g. data export to USB Stick)
	* 3.3V VDD_USB generation
		* Linear regulator, only powered if V_USB available
	
.. image:: ../pictures/schematic/WABEsense_Datalogger_Schematic-002.png
  :width: 800
  :alt: Datalogger power
  
Internal parts (`AT25SF321B`_ (FLASH) / `BME280`_)
==================================================
	* `AT25SF321B`_ FLASH data/configuration storage
		* 4 MB FLASH
		* connected over SPI1 Interface to microcontroller
	* `BME280`_ sensor
		* pressure, temperature, humidity
		* connected over SPI1 Interface to microcontroller
		
.. image:: ../pictures/schematic/WABEsense_Datalogger_Schematic-003.png
  :width: 800
  :alt: Datalogger internal (FLASH / BME280)

Microcontroller
===============
	* JTAG connector (J4)
	* V_Supply measurement path
		* ON/OFF switchable to reduce current consumption
	* SD-Card insert detection
	* Microcontroller power connections

.. image:: ../pictures/schematic/WABEsense_Datalogger_Schematic-004.png
  :width: 800
  :alt: Datalogger microcontroller
  
USB & ESD Protection
====================
	* USB Host/Device mode switching (OTG_Device signal)
	* ESD Protection for USB D+/D-
	
.. image:: ../pictures/schematic/WABEsense_Datalogger_Schematic-005.png
  :width: 800
  :alt: Datalogger USB & ESD Protection
  
In/outputs
==========
	* 2 x 16 pin connector
		* 4x Analog 0-5 V
		* 2x Analog 0-3.3 V (VDDA)
		* 7x Digital In/Out
		* SPI interface or additional 4x Digital In/Out
		* I2C interface or additional 2x Digital In/Out
	* 2 x Sensor Connectors (J6, J7 as `1935187`_ screw connector)
		* 17V Powered Sensors
		* 2 x 5V analog output of the sensor
	* HMI IN/OUTPUTS
		* USR_BT1 via J8 (`1778625`_)
		* USR_BT2 via J9 (`1778625`_)
		* Two color LED (Red/Green) via J10 (`1778638`_)

.. image:: ../pictures/schematic/WABEsense_Datalogger_Schematic-006.png
  :width: 800
  :alt: Datalogger in/outputs

Download the full schematic as pdf: :download:`Schematic pdf as file <../pictures/schematic/WABEsense_Datalogger_Schematic.pdf>`

Links
=====
.. _BME280: https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/
.. _AT25SF321B: https://www.dialog-semiconductor.com/sites/default/files/2021-04/DS-AT25SF321B-179E-062020.pdf
.. _1778625: https://www.phoenixcontact.com/online/portal/ch?uri=pxc-oc-itemdetail:pid=1778625&library=chde&tab=1
.. _1778638: https://www.phoenixcontact.com/online/portal/ch?uri=pxc-oc-itemdetail:pid=1778638&library=chde&tab=1
.. _1935187: https://www.phoenixcontact.com/online/portal/de?uri=pxc-oc-itemdetail:pid=1935187&library=dede&tab=1

.. target-notes::